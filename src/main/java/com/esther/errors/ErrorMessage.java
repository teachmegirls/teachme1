package com.esther.errors;

import java.util.Date;

import com.esther.enums.ErrorCode;

public class ErrorMessage {
	private long id;
	private ErrorCode errorCode;
	private String status;
	private String description;
	private Date date;

	public ErrorMessage(ErrorCode errorCode, String description) {
		this.errorCode = errorCode;
		this.description = description;
		this.date = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
