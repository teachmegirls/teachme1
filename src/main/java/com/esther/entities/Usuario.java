package com.esther.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.esther.enums.Conta;

@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne(mappedBy = "usuario")
	private Pagamento pagamento;
	@OneToMany(mappedBy = "usuario")
	private List<Aula> aulas;
	private String nome;
	private String email;
	private String senha;
	@Column(name = "data_nascimento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataNascimento;
	@Enumerated(EnumType.STRING)
	private Conta conta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Aula> getAulas() {
		return aulas;
	}

	public void setAulas(List<Aula> aulas) {
		this.aulas = aulas;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Usuario() {

	}

	private Usuario(UsuarioBuilder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.email = builder.email;
		this.senha = builder.senha; 
		this.dataNascimento = builder.dataNascimento;
		this.conta = Conta.valueOf(builder.conta);
		this.pagamento = (Pagamento) builder.pagamento;
		this.aulas = builder.aulas;
	}

	public static class UsuarioBuilder {
		private Long id;
		private Object pagamento;
		private List<Aula> aulas;
		private String nome;
		private String email;
		private String senha;
		private Date dataNascimento;
		private String conta;

		public UsuarioBuilder addId(Long id) {
			this.id = id;
			return this;
		}

		public UsuarioBuilder addPagamento(Long pagamento) {
			this.pagamento = pagamento;
			return this;
		}

		public UsuarioBuilder addAulas(List<Aula> aulas) {
			this.aulas = aulas;
			return this;
		}

		public UsuarioBuilder addNome(String nome) {
			this.nome = nome;
			return this;
		}

		public UsuarioBuilder addEmail(String email) {
			this.email = email;
			return this;
		}

		public UsuarioBuilder addSenha(String senha) {
			this.senha = senha;
			return this;
		}

		public UsuarioBuilder addDataNascimento(Date dataNascimento) {
			this.dataNascimento = dataNascimento;
			return this;
		}

		public UsuarioBuilder addConta(String conta) {
			this.conta = conta;
			return this;
		}

		public Usuario build() {
			return new Usuario(this);
		}
	}
}
