package com.esther.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.esther.entities.Usuario;
import com.esther.enums.ErrorCode;
import com.esther.errors.ErrorMessage;
import com.esther.repositories.UsuarioRepository;
import com.esther.vo.UsuarioVO;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> criarUsuario(@RequestBody UsuarioVO usuarioVO){
		try {
				Usuario usuario = new Usuario.UsuarioBuilder()
						.addNome(usuarioVO.getNome())
						.addEmail(usuarioVO.getEmail())
						.addSenha(usuarioVO.getSenha())
						.addDataNascimento(usuarioVO.getDataNascimento())
						.addConta(usuarioVO.getConta())
						//.addAulas(usuarioVO.getAulas())
						//.addPagamento(usuarioVO.getPagamentoId())
						.build();
				
				this.usuarioRepository.save(usuario);
				
				return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(ErrorCode.USER_002, e.getMessage()));
		}
	}
}
