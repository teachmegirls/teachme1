package com.esther.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.esther.entities.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}
