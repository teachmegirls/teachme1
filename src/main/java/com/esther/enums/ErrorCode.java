package com.esther.enums;

public enum ErrorCode {
	USER_001("NULLPOINTER"),
	USER_002("SERVER"),
	USER_003("USER NOT FOUND");

	private String description;

	ErrorCode(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
